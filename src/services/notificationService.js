const config = require("../config")
const { getReportEmbed } = require('../services/embedService')


module.exports = {
    startReactionWatcher(message, reactionList) {
        let notifyUser = true
        const notificationTimeout = config.NOTIFICATION_RULES().timeoutForNotifications
        let notifyCollector = message.createReactionCollector({ time: notificationTimeout })

        notifyCollector.on('collect', async (reaction, user) => {
            if (user.id == message.author.id && reaction._emoji.name == "🗑️") {
                notifyUser = false
                message.author.send("Benachrichtungen deaktiviert!")
            }

            if (!notifyUser) return

            // This prevents users from spamming with readding their reaction
            if (!choosenEmojis.includes(reaction._emoji.name) || (reactionList[reaction._emoji.name].includes(user.id))) return

            reactionList[reaction._emoji.name].push(user.id)
            const reportEmbed = await getReportEmbed(message, mentionedRoleId, reactionList, user, reaction)
            message.author.send({ embeds: [reportEmbed] })
        })
    }
}