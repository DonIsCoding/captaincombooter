# Captain Combooter

Discord Bot (duh) for CC Server (Private). 

## Features

Ask server members to play e.g. video games and get notified on answers. Customize a role on and if this role is mentioned in a message the bot will add customizable emojis. 

When you ping the set role then the bot will react to your message with the emojis and also will ask you if it should notify on reactions.

![Bot asked if it should notify the user](/docs/images/bot_ask_for_notification.png)

If you press "Ja" you will be asked for which emojis you want to be notified about. It also lists the already choosen emojis

![Bot asked if it should notify the user](/docs/images/bot_ask_for_which_notification.png)

After clicking on "Speichern" it will notifiy you per DM

![Bot asked if it should notify the user](/docs/images/bot_notification.png)

## Setup

### Development 

This bot requires node v16 or higher

1. Clone the repository and navigate into it
2. Copy the .env.sample file and paste it into the src/ folder
3. Rename it to ".env" and put in your creds
4. Copy ```src/config.json``` and paste it into src/ and rename it to ```config.dev.json```. Insert configs for your dev server there
5. Run ```npm i``` in the src/ folder
6. Run the bot with ```npm run dev```


### Deployment
#### **Docker run**
Have docker installed.
1. Clone repo
2. Run ```docker build -t cc/combooter .```
3. Run 
```bash
docker run --name combooter -e BOT_TOKEN='<YOUR_BOT_TOKEN>' --restart unless-stopped -d cc/combooter
```
4. For some logs run ```docker logs -tf combooter```

#### **Docker-compose**
Have docker + docker-compose installed
```yml
version: "3"
services:
  combooter:
    image: combooter
    build: ./captaincombooter
    container_name: combooter
    restart: unless-stopped
    environment:
     - BOT_TOKEN=<Your_Token_Here>
```
Then run this in the folder with the docker-compose.yml file
```bash
# for standalone docker compose
docker-compose up -d --build
# for docker compose plugin
docker compose up -d --build
```